(ns monitorat.realtime
  (:require [cheshire.core :as json]
            [monitorat.mongo :as mongo]
            [clj-time.core :as time]
            [taoensso.timbre :as log]
            [clojure.set :as set])
  (:use [monitorat.common :only [time-point-of power-set]])
  )

(defn- divide [a b scale round-mode]
  (let [mode-mapping {:half-up BigDecimal/ROUND_HALF_UP,
                      :up BigDecimal/ROUND_UP,
                      :half-down BigDecimal/ROUND_HALF_DOWN,
                      :down BigDecimal/ROUND_DOWN}]
    (.divide (bigdec a)
             (bigdec b)
             scale
             (get mode-mapping round-mode))))

(defn- sum [values]
  (reduce #(+ %1 %2) 0 values)
  )

(defn- max-of-coll [values]
  (condp = (count values)
    0 nil,
    1 (first values)
    (reduce max values)))

(defn- min-of-coll [values]
  (condp = (count values)
    0 nil,
    1 (first values)
    (reduce min values)))


(defn- avg [values]
  (float (divide (sum values) (count values) 0 :half-up)))

(def AGGREGATORS
  {:sum sum,
   :max max-of-coll,
   :min min-of-coll,
   :avg avg,
   :count count
   :latest last
   })


(defn- overlay-a
  "overlay aggregation, return the data which only updated by new-a"
  [base-a new-a]
  (let [op-map {:sum (fn [a b] (+ (:sum a) (:sum b)))
                :count (fn [a b] (+ (:count a) (:count b)))
                :min (fn [a b] (min (:min a) (:min b)))
                :max (fn [a b] (max (:max a) (:max b)))
                :latest (fn [a b] (:latest b))
                :avg (fn [a b]
                       (float (divide
                               (+ (:sum a) (:sum b))
                               (+ (:count a) (:count b))
                               0
                               :half-up)))}]
    (reduce (fn [result [a-name a-value]]
              (if (contains? op-map a-name)
                (assoc result a-name
                       (if (get base-a a-name)
                         (apply (get op-map a-name) [base-a new-a])
                         a-value
                         ))
                result
                ))
            {}
            new-a)))

(defn- prepare-tsd [chunk]
  (for [s (:data-seq chunk)
        interval ["1m" "5m" "15m" "1h"]
        :let [tsd (json/parse-string s true)
              timestamp (:timestamp tsd)
              dimensions (:dimensions tsd)]]
    (-> tsd
        (assoc :interval interval)
        (assoc :time-point (time-point-of interval timestamp))
        (update-in [:dimensions] #(or % {}))
        (update-in [:dimensions] #(merge (sorted-map) %))
        (update-in [:value] float)
        )))

(defn- group-tsd [tsd-seq]
  (group-by (fn [tsd]
              (merge (sorted-map)
                     (select-keys tsd [:user-id :metric-name :interval :time-point :dimensions])))
            tsd-seq))

(defn- aggregate [tsd-seq]
  (reduce (fn [result [a-name, a-fn]]
            (assoc result a-name (a-fn (map #(get % :value)  tsd-seq))))
          {}
          AGGREGATORS))

(defn- update-metric [tsd-group]
  (doseq [[group-index group-data] tsd-group]
    (mongo/swap-aggregation group-index
                            (fn [old-value]
                              (overlay-a old-value (aggregate group-data))))))

(defn realtime [chunk]
  (-> chunk
      (prepare-tsd )
      (group-tsd)
      (update-metric)))


;;; for test
(def test-data-1 [{:user-id "user001",
                   :metric-name "system",
                   :time-point (time/date-time 2013 12 20 10 0)
                   :dimensions {}
                   :interval "5m"
                   :value 10}
                  {:user-id "user001",
                   :metric-name "system",
                   :time-point (time/date-time 2013 12 20 10 0)
                   :dimensions {}
                   :interval "5m"
                   :value 20}
                  ])

(def test-chunk {:key (time/date-time 2012 1 19 5 1)
                  :data-seq ["{ \"metric-name\" : \"tsdinstance\",\"value\" : 100, \"timestamp\" : \"2012-01-19T19:05:01-05:00\"  }"]
                  })
