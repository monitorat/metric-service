(ns monitorat.common
  (:require [monitorat.import :as imp])
  (:import [com.google.common.collect Sets MinMaxPriorityQueue]
           [java.util Set Map]
           )
  )

(imp/require-all)

(def DEFAULT-TZ "Asia/Shanghai")

(defn trunc-time
  ([d] (trunc-time d 60))
  ([d seconds]
     (let [epoch-mills (time-coerce/to-long d)]
       (time-coerce/from-long
        (- epoch-mills
           (mod epoch-mills (* 1000 seconds))))
       )))

(defn format-time
  ([d format] (format-time d format DEFAULT-TZ))
  ([d format tz]
     (time-format/unparse
      (time-format/formatter format (time/time-zone-for-id tz))
      d)))


;;; power-set for set and map
(defmulti power-set class)

(defmethod power-set clojure.lang.Seqable [s]
  (set (Sets/powerSet (set s))))

(defmethod power-set Map [m]
  (println "powser set map")
  (if (= 0 (count m))
    [{}]
    (map
     #(select-keys m %) 
     (power-set (keys m)))))

(prefer-method power-set Map clojure.lang.Seqable)


;;; interval ;;;
(defn interval-seconds [interval]
  (condp = interval
    "1m" 60,
    "5m" 300,
    "15m" 900,
    "1h" 3600
    ))

(defn time-point-of [interval timestamp]
  (trunc-time timestamp
              (interval-seconds interval)))
