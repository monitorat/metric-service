(ns monitorat.metadata
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [monitorat.restful :as restful]
            [taoensso.timbre :as log]
            [monger.collection :as mgc]
            [clojure.string :as string])
  (:use [ring.util.response :only [status response] ]))


(defn- clj2db [_name]
  (if-let [s (and (or (keyword? _name) (string? _name)) (name _name))]
    (let [recover-fn (if (keyword? _name) keyword str)]
      (recover-fn (string/replace (name _name) "-" "_"))
      )
    _name))


(defn to-dbnaming [obj]
  (into {}
        (map (fn [[key value]]
               [(clj2db key)
                (if (map? value) (to-dbnaming value) value)])
             obj)))

(defn validate-meta [meta]
  (and (get-in meta [:metadata :net-ifs]) (get-in meta [:metadata :disks])))

(defn save-meta [meta]
  (mgc/update "metric_metadata"
              (select-keys meta [:customer-id :category :instance])
              (to-dbnaming meta)
              :upsert true))


(defn handler [r]
  (let [{:keys [customer-id body]} r]
    (or 
     (some (fn [metadata]
             (cond
              (not (validate-meta metadata))
              (do
                (log/warn "illegal metadata:" metadata)
                (restful/bad-request "illegal-format"  "metadata is illegal"))

              (not (save-meta metadata))
              (do
                (log/warn "internal error for" metadata)
                (restful/internal-error))

              :else nil
              ))
           (json/parse-stream (io/reader body) true))
     (status (response "") 204))))
