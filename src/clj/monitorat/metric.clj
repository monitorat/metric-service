(ns monitorat.metric
  (:refer-clojure :exclude [sort find])
  (:require [monger.collection :as mgc]
            [monger.core :as mg]
            [monger.joda-time]
            [clj-time.core :as time]
            [cheshire.core :as json]
            [taoensso.timbre :as log]
            [monitorat.restful :as restful])
  (:use [compojure.core :only [let-request]]
        [clj-time.coerce :only [from-long to-long]]
        [clj-time.core :only [date-time]]
        [monger.query :only [with-collection find sort limit]]))


(def test-r {:params {:name "sys.cpu.usage"
                      :interval "5m"
                      :aggregator "sum"
                      :start 1383264000
                      :end   1383267600
                      }
             :user-id "user001"})

(defn to-seconds [interval]
  (condp = interval
    "5m" 300
    ))

(defn interval-seq [sorted-seq step key-fn default-fill-fn]
  (loop [result []
         expect (key-fn (first sorted-seq))
         s sorted-seq]
    (let [next-e (first s)]
      (if-not (seq s)
        result
        (cond
         (= expect (key-fn next-e))
         (recur (conj result next-e) (+ expect step) (rest s))

         (> (key-fn next-e) expect )
         (recur (conj result (default-fill-fn expect)) (+ expect step) s)

         (< (key-fn next-e) expect) ;; skip this element
         (recur result expect (rest s))
         )))))

(defn handler [r]
  (let [user-id (:user-id r)]
    (let-request [[name start end aggregator interval & dimensions] r]
      (if-not (and name start end aggregator interval)
        (restful/bad-request "illegal-parameters" "necessary parameters is necessary")
        (let [start (from-long (* 1000 (Long. start)))
              end (from-long (* 1000 (Long. end)))
              dimensions (merge (sorted-map) dimensions)]
          (json/generate-string
           (interval-seq 
            (map (fn [x] [(long (/ (to-long (:time_point x)) 1000)) (:value x)])
                 (let [criteria {:user_id user-id
                                 :metric_name name
                                 :interval interval
                                 :aggregator aggregator
                                 :time_point {"$gte" start, "$lt" end}
                                 :dimensions dimensions
                                 }]
                   (log/debug "metric query criteria: " criteria)
                   (with-collection "tsd_5m"
                     (find criteria)
                     (sort (array-map :timepoint 1))
                     )))
            (to-seconds interval)
            #(first %)
            (fn[x] [x 0])
            )))))))
