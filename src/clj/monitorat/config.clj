(ns monitorat.config
  (:require [taoensso.timbre :as log]
            [taoensso.timbre.appenders.rotor :as rotor])
  (:use [environ.core :only [env]])
  )

(defn- is-number? [s]
  (try
    (Float. s) 
    true
    (catch Exception e false)) )

(defn- slice 
  ([s start] (slice s start (count s)))
  ([s start end] 
   (letfn [(stand-index [i]
                        (if (< i 0) (+ i (count s)) i)
                        )]
     (subs s (stand-index start) (stand-index end))) ))


(defn- parse-size [s]
  (let [[n unit] (if (not (is-number? (slice s -1)))
                   [(slice s 0 -1) (slice s -1)]
                   [s ""] ) ]
    (* (Float. n) 
       (condp = unit
         "k" 1024
         "m" (* 1024  1024)
         "g" (* 1024 1024 1024)
         1))))

(defonce configs (atom {}))

(defn set-logging [options]
  (when (env :mygod-env-root)
    ;;; effective only for mygod environment
    (log/set-config! 
      [:appenders :standard-out]
      {:enabled? false})
    (log/set-config! 
      [:appenders :rotor]
      {:min-level (or (and (:level options) (keyword (:level options))) 
                      :debug)
       :enabled? true,
       :async? false,
       :fn rotor/appender-fn}
      ) 
    (log/set-config!
      [:shared-appender-config :rotor]
      {:path (str (env :mygod-env-root) "/var/" (:path options))
       :max-size (or (and (:max-size options) (parse-size (:max-size options))) 
                     (* 10 1024 1024)) 
       :backlog  (or (and (:backlog options) (Integer. (:backlog options))) 
                     5)
       }
      )))

