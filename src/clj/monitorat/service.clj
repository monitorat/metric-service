(ns monitorat.service
  (:gen-class)
  (:require (monitorat [tsd :as tsd]
                       [restful :as restful]
                       [metric :as metric]
                       [metadata :as metadata]
                       [mongo :as mongo])
            [clojure-ini.core :as ini]
            [compojure.handler :as handler]
            [taoensso.timbre :as log]
            [clojure.tools.nrepl.server :as nrepl]
            [ring.adapter.jetty :as jetty]
            )
  (:use [compojure.core]
        [clojure.tools.cli :only [cli]]
        [monitorat.config :only [configs, set-logging]])
  )

;;; REST API ;;;
(defn auth-middleware [handler]
  (fn [req]
    (handler (assoc req :user-id "user001"))
    ))


(defroutes app-routes
  (POST "/tsds" [:as r] (tsd/handler r))
  (GET "/metric" [:as r] (metric/handler r))
  (POST "/metadata" [:as r] (metadata/handler r))
  )

(def app (->
          (handler/api app-routes)
          (restful/wrap-gzip-request)
          (auth-middleware)))

(defn ring-app-init []
  (println "initialize ring application...")
  (mongo/init-mongo {:host "localhost" :port 27017}))

;;; main ;;;
(defn -main [& args]
  (let [[opts _ help]
        (cli args
             ["-c" "--config" "the config file"]
             )]
    
    (when-not (:config opts)
      (log/error "config option is necessary")
      (System/exit 1))

    (swap! configs merge (ini/read-ini (:config opts) :keywordize? true :comment-char \#))
    (set-logging (:logging @configs))
    (mongo/init-mongo {:host (:mongo-host @configs),
                       :port (Integer. (:mongo-port @configs))})
    (log/debug "starting server...")
    (nrepl/start-server :port 5555)
    (jetty/run-jetty app {:port (Integer. (:port @configs))} )))
