(ns monitorat.mongo
  (:require (monger [core :as mg]
                    [collection :as mc])
            [taoensso.timbre :as log]
            [clojure.string :as str]
            ))

(defn- clj2db [_name]
  (if-let [s (and (or (keyword? _name) (string? _name)) (name _name))]
    (let [recover-fn (if (keyword? _name) keyword str)]
      (recover-fn (str/replace (name _name) "-" "_"))
      )
    _name))

(defn- db-name [obj]
  (into {}
        (map (fn [[key value]]
               [(clj2db key)
                (if (map? value) (db-name value) value)])
             obj)))

(defn set-mongodb!
  ([] (set-mongodb! "127.0.0.1" 27017))
  ([host port]
     (mg/connect! {:host host, :port port})
     (mg/set-db! (mg/get-db "monitorat"))
     ))


(defn init-mongo [ mongo-opts]
  (let [opts (merge {:host "127.0.0.1"
                     :port 27017}
                    mongo-opts)]
    (mg/connect! (select-keys mongo-opts [:host :port]))
    (mg/set-db! (mg/get-db "monitorat"))
    ))

(defn find-aggregation
  ([keys] (find-aggregation keys [:avg :sum :count :min :max :latest]))
  ([keys aggregators]
     (reduce (fn [result agg]
               (if-let [find-result (mc/find-one-as-map (str "tsd_" (:interval keys))
                                                        (db-name (assoc keys :aggregator agg))
                                                        {:_id 0, :value 1})]
                 (assoc result agg (:value find-result))
                 result))
             {}
             aggregators)))

(defn swap-aggregation
  "swap metric's aggregation vales.
key is the criteria key of metric {:user-id xxx, :metric-name xxx, ....}"
  [keys new-value-fn]
  (doseq [[agg new-value] (new-value-fn (find-aggregation keys))]
    (log/debug (format "swap aggregation values: keys=%s, aggregator=%s, value=%s" keys agg new-value))
    (mc/update (str "tsd_" (:interval keys))
               (db-name (assoc  keys :aggregator agg))
               {"$set" {:value new-value}}
               :upsert true
               )))


;;;; for test
(defn update-fn [a-values]
  {:count (if-let [old-count (get a-values :count) ](inc old-count) 0)}
  )
