#!/usr/bin/env python

import sys, os, time, atexit
from signal import SIGTERM 
from os.path import join, basename, dirname
import shlex, subprocess

class Daemon(object):
    """
    A generic daemon class.
    
    Usage: subclass the Daemon class and override the run() method
    """
    def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null', commandline=None):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
        self.commandline = commandline

        def _create_dir(file_path):
            d = dirname(file_path)
            if not os.path.exists(d):
                os.makedirs(d)

        _create_dir(pidfile)
        _create_dir(stdin)
        _create_dir(stdout)
        _create_dir(stderr)
    
    def daemonize_subprocess(self):
        if not self.commandline:
            sys.stderr.write("have not specific command line to execute")
            sys.exit(1)

        p = subprocess.Popen(shlex.split(self.commandline), stdout=file(self.stdout, 'w') , stdin=file('/dev/null','r'), stderr=file(self.stderr, 'w'))
        file(self.pidfile,'w+').write("%s\n" % p.pid)

    
    def daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced 
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        try: 
            pid = os.fork() 
            if pid > 0:
                # exit first parent
                sys.exit(0) 
        except OSError, e: 
            sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)
    
        # decouple from parent environment
        os.chdir("/") 
        os.setsid() 
        os.umask(0) 
    
        # do second fork
        try: 
            pid = os.fork() 
            if pid > 0:
                # exit from second parent
                sys.exit(0) 
        except OSError, e: 
            sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1) 
    
        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        si = file(self.stdin, 'r')
        so = file(self.stdout, 'a+')
        se = file(self.stderr, 'a+', 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())
    
        # write pidfile
        atexit.register(self.delpid)
        pid = str(os.getpid())
        file(self.pidfile,'w+').write("%s\n" % pid)
    
    def delpid(self):
        os.remove(self.pidfile)

    def start(self):
        """
        Start the daemon
        """
        # Check for a pidfile to see if the daemon already runs
        try:
            pf = file(self.pidfile,'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
    
        if pid:
            message = "pidfile %s already exist. Daemon already running?\n"
            sys.stderr.write(message % self.pidfile)
            sys.exit(1)
        
        if self.commandline:
            self.daemonize_subprocess()
        else:
            # Start the daemon
            self.daemonize()
            self.run()

    def stop(self):
        """
        Stop the daemon
        """
        # Get the pid from the pidfile
        try:
            pf = file(self.pidfile,'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
    
        if not pid:
            message = "pidfile %s does not exist. Daemon not running?\n"
            sys.stderr.write(message % self.pidfile)
            return # not an error in a restart

        # Try killing the daemon process    
        try:
            while 1:
                os.kill(pid, SIGTERM)
                time.sleep(0.1)
        except OSError, err:
            err = str(err)
            if err.find("No such process") > 0:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print str(err)
                sys.exit(1)

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def run(self):
        """
        You should override this method when you subclass Daemon. It will be called after the process has been
        daemonized by start() or restart().
        """

class SimpleDaemon(Daemon):

    def __init__(self, run_fn, **kargs):
        self._run_fn = run_fn
        super(SimpleDaemon, self).__init__(**kargs)

    def run(self):
        self._run_fn()

def daemon(daemon_dir, run_fn=None, commandline=None):
    exec_name = basename(sys.argv[0])
    
    if len(sys.argv) != 2:
        print " usage: %s <start|stop|restart>" % exec_name
        sys.exit(1)

    if commandline:
        dm = Daemon(pidfile = join(daemon_dir, exec_name+".pid"),  stdout=join(daemon_dir, exec_name+".stdout"), stderr=join(daemon_dir, exec_name+".stderr"), commandline=commandline)
    else:
        dm = SimpleDaemon(run_fn, pidfile = join(daemon_dir, exec_name+".pid"),  stdout=join(daemon_dir, exec_name+".stdout"), stderr=join(daemon_dir, exec_name+".stderr"))
    action = sys.argv[1]
    if action == 'start':
        dm.start()
    elif action == 'stop':
        dm.stop()
    elif action == 'restart':
        dm.restart()
    else:
        print "%s is not supportted action" % action