(ns monitorat.mongo-test
  (:require monger.joda-time
            [clj-time.core :as time])
  (:use monitorat.mongo)
  )

(def test-keys {:user-id "user001"
                         :metric-name "server/monitorat-beta/cpu/user"
                         :interval "5m",
                         :dimensions {}
                         :time-point (time/date-time 2013 12 20 12 10)})

(defn test-find-tsd-aggregator []
  (init-mongo {:host "monitorat-beta" :port 27017})
  (find-tsd-aggregation  test-keys))

(defn test-swap-tsd-aggregator []
  (init-mongo {:host "monitorat-beta" :port 27017})
  (swap-tsd-aggregation test-keys (fn [aggs] (reduce (fn [result [agg value]]
                                                       (assoc result agg (inc value)))
                                                     {}
                                                     aggs
                                                     ))))
