(defproject metric-service "0.1.0"
  :url "http://monitorat.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [ring/ring-core "1.2.0"]
                 [ring/ring-jetty-adapter "1.2.0"]
                 [compojure "1.1.5"]
                 [cheshire "5.2.0"]
                 [com.taoensso/timbre "2.7.1"]
                 [clj-chunk-buffer "0.3.1"]
                 [org.apache.hadoop/hadoop-core "0.20.2"]
                 [org.clojure/tools.cli "0.2.4"]
                 [org.clojure/tools.nrepl "0.2.3"]
                 [clj-time "0.6.0"]
                 [clj-http "0.7.7"]
                 [clojure-ini "0.0.1"]
                 [com.novemberain/monger "1.5.0"]
                 [environ "0.4.0"]
                 ]
  :ring {:handler monitorat.service/app, :init monitorat.service/ring-app-init}
  :plugins [[lein-ring "0.8.8"]]
  :source-paths ["src/clj"]
  :aot :all
  :main monitorat.service)
